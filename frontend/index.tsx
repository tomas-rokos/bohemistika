import * as React from 'react';
import { render } from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';

import 'antd/dist/antd.css';

import App from './app/app';

render(
    <Router>
        <App />
    </Router>
    , document.getElementById('root'));

