import * as React from 'react';
import styled from 'styled-components';

import * as Vector from '../math/vector';

const Elem = styled.div`
    position: absolute;
    border: 0.5px solid black;
    transform-origin: 0 100%;
`;

export function Line(props: {
    from: { x: number, y: number };
    to: { x: number, y: number };
}) {
    const [origin, end] = props.from.x <= props.to.x ? [props.from, props.to] : [props.to, props.from];
    const length = Vector.length(Vector.substract(end, origin));

    const style = { left: origin.x, top: origin.y, width: length };
    if (origin.y !== end.y) {
        const angle = Math.atan((end.y - origin.y) / (end.x - origin.x));
        style['transform'] = `rotate(${angle}rad)`;
    }
    return (
        <Elem
            style={style}
        />
    );
}

export default Line;
