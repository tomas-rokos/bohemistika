import * as _ from 'lodash';
import * as Vector from '../../math/vector';

export interface Token {
    key: string;
    text: string;
    pos: Vector.Vector;
    type?: string;
    parent?: string;

    // assigned when rendered
    ref?: any;
}

export interface Connector {
    from: Vector.Vector;
    to: Vector.Vector;
}

export interface State {
    selectionSet: string[];
    isDragging: boolean;
    draggingPos: Vector.Vector;
    tokens: Token[];
    connectors: Connector[];
}

export class SceneManager {
    state: State = {
        selectionSet: [],
        isDragging: false,
        draggingPos: { x: 0, y: 0 },
        tokens: [],
        connectors: [],
    };
    lastKey = '';
    lastAddedToSelection = false;
    moved = false;

    setTokens(tokens: Token[]) {
        this.state.tokens = tokens;
        this.calcConnectors();
    }

    setRef(key: string, ref: any) {
        if (ref == null) {
            return;
        }
        const tkn = _.find(this.state.tokens, {'key': key });
        if (tkn.ref == null) {
            tkn.ref = ref;
        }
    }

    mouseDown(key: string, pos: Vector.Vector): State {
        this.lastKey = key;
        if (this.state.selectionSet.indexOf(key) !== -1) {
            this.lastAddedToSelection = false;
        } else {
            this.lastAddedToSelection = true;
            this.state.selectionSet.push(key);
        }
        this.state.isDragging = true;
        this.state.draggingPos = pos;
        this.moved = false;
        return this.state;
    }
    mouseMove(pos: Vector.Vector): boolean {
        if (this.state.isDragging === false) {
            return false;
        }
        const deltaX = pos.x - this.state.draggingPos.x;
        const deltaY = pos.y - this.state.draggingPos.y;
        if (deltaX === 0 && deltaY === 0) {
            return false;
        }
        this.moved = true;
        this.state.tokens.forEach((tkn) => {
            if (this.state.selectionSet.indexOf(tkn.key) === -1) {
                return;
            }
            tkn.pos.x += deltaX;
            tkn.pos.y += deltaY;
        });
        this.state.draggingPos = pos;
        this.calcConnectors();
        return true;
    }
    mouseUp( pos: Vector.Vector): State {
        if (this.state.isDragging) {
            if (this.moved) {
                if (this.lastAddedToSelection) {
                    if (this.state.selectionSet.length === 1) {
                        this.state.selectionSet = [];
                    }
                }
            } else {
                if (this.lastAddedToSelection) {
                } else {
                    this.state.selectionSet = _.without(this.state.selectionSet, this.lastKey);
                }
            }
        } else {
            this.state.selectionSet = [];
        }
        this.state.isDragging = false;
        this.state.draggingPos = pos;
        return this.state;
    }
    calcConnectors() {
        this.state.connectors = [];
        this.state.tokens.forEach( (tkn) => {
            if (tkn.parent == null) {
                return;
            }
            const tknParent = _.find(this.state.tokens, {'key': tkn.parent });
            const tknPos = this.connectionPoint(tkn, tknParent.pos);
            const tknParentPos = this.connectionPoint(tknParent, tkn.pos);
            this.state.connectors.push( { from: tknParentPos, to: tknPos});
        });
    }
    private connectionPoint(tkn: Token, from: Vector.Vector): Vector.Vector {
        if (tkn.ref == null) {
            return tkn.pos;
        }
        if (tkn.type === 'subject') {
            return {x: tkn.pos.x + tkn.ref.offsetWidth / 2, y: tkn.pos.y + tkn.ref.offsetHeight / 2 };
        }
        let result = {x: tkn.pos.x, y: tkn.pos.y + tkn.ref.offsetHeight - 1};
        if (tkn.pos.x < from.x) {
            result = Vector.add(result, {x: tkn.ref.offsetWidth, y: 0});
        }
        return result;
    }
}
