import * as _ from 'lodash';
import * as React from 'react';
import styled from 'styled-components';

import DocList from '../components/doc-list';

import useApiCall from '../store/use-api-call';

const Elem = styled.div`
    width: 100%;
    height: 100vh;
    a {
        color: black;
        padding: 0 5px;
        text-decoration: underline;
    }

`;

export const Index = (props) => {
    const data = useApiCall('notes.py');

    return (
        <Elem>
            <h1>Myšlenkář je virtuální zpravodajská služba.</h1>
            Zpravodajská služba kdykoliv připravená zpracovat dodané informace, obohatit je o volně dostupné informace a
            vytvořit
            zpravodajskou zprávu, která bude obsahovat objektivní prolnutí všech těchto informací.
            Máte možnost nechat cloudové technologie jako je
            <a href='https://cs.wikipedia.org/wiki/Velká_data'>zpracování velkého množství dat</a>,
            <a href='https://cs.wikipedia.org/wiki/Zpracován%C3%AD_přirozeného_jazyka'>zpracování přirozeného
                jazyka</a> a
            <a href='https://cs.wikipedia.org/wiki/Prognóza'>strukturální analýzu</a>
            pracovat pro své potřeby. Portál využívá techniky
            <a href='https://cs.wikipedia.org/wiki/Zpravodajstv%C3%AD_z_otevřených_zdrojů'>zpravodajství z veřejných
                zdrojů</a>.
            <br/><br/>
            <DocList docs={data} />
        </Elem>
    );
};

export default Index;
