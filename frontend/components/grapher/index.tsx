import * as _ from 'lodash';
import * as React from 'react';
import styled from 'styled-components';
import Line from '../line';

import Node from './node';
import Subject from './subject';

import { State, SceneManager } from './scene-manager';

const Comp = styled.div`
    position: relative;
    overflow: scroll;

    width: 100%;
    height: 100%;

    .content {
        position: absolute;
    }
`;

const BoundingBox = styled.div`
    border: 1px dashed grey;
    position: absolute;
`;

export class Grapher extends React.Component<{
    tokens;
}, State> {
    scene = new SceneManager();
    constructor(props: any) {
        super(props);

        this.scene.setTokens(props.tokens);
        this.state = this.scene.state;
    }

    listenerOnMouseDown = (id) => (e) => {
        this.setState(this.scene.mouseDown(id, { x: e.screenX, y: e.screenY }));
    }

    listenerOnMouseMove = (e) => {
        if (this.scene.mouseMove({ x: e.screenX, y: e.screenY })) {
            this.setState(this.scene.state);
        }
    }
    listenerOnMouseUp = (e) => {
        this.setState(this.scene.mouseUp({ x: e.screenX, y: e.screenY }));
    }
    componentDidMount() {
        window.addEventListener('mousemove', this.listenerOnMouseMove);
        window.addEventListener('mouseup', this.listenerOnMouseUp);
    }
    componentWillUnmount() {
        window.removeEventListener('mousemove', this.listenerOnMouseMove);
        window.removeEventListener('mouseup', this.listenerOnMouseUp);
    }
    componentWillReceiveProps(nextProps: Readonly<{ tokens }>, nextContext: any): void {
        this.scene.setTokens(nextProps.tokens);
        this.setState(this.scene.state);
        const self = this;
        setTimeout( () => {
            self.scene.calcConnectors();
            self.setState(this.scene.state);
        }, 10);
    }
    render() {
        const tokens = this.state.tokens.map((tkn, idx) => {
            return tkn.type === 'subject' ?
                (
                    <Subject
                        ref={(e) => this.scene.setRef(tkn.key, e)}
                        key={tkn.key}
                        text={tkn.text}
                        onMouseDown={this.listenerOnMouseDown(tkn.key)}
                        pos={tkn.pos}
                        selected={this.state.selectionSet.indexOf(tkn.key) !== -1}
                    />
                ) : (
                    <Node
                        ref={(e) => this.scene.setRef(tkn.key, e)}
                        key={tkn.key}
                        text={tkn.text}
                        onMouseDown={this.listenerOnMouseDown(tkn.key)}
                        pos={tkn.pos}
                        selected={this.state.selectionSet.indexOf(tkn.key) !== -1}
                    />

                );
        });
        const connectors = this.state.connectors.map( (cn, idx) => <Line key={idx} from={cn.from} to={cn.to} />);
        let bbox = null;
        if (this.state.selectionSet.length > 1) {
            let top = 100000, left = 100000, bottom = -100000, right = -100000;
            this.state.selectionSet.forEach((key: string) => {
                const tkn = _.find(this.state.tokens, { 'key': key });
                top = _.min([top, tkn.ref.offsetTop]);
                left = _.min([left, tkn.ref.offsetLeft]);
                bottom = _.max([bottom, tkn.ref.offsetTop + tkn.ref.offsetHeight]);
                right = _.max([right, tkn.ref.offsetLeft + tkn.ref.offsetWidth]);
            });
            const bounds = { top: top - 5, left: left - 5, width: right - left + 10, height: bottom - top + 10 };
            bbox = <BoundingBox style={bounds} />;
        }
        return (
            <Comp>
                <div className='content' style={{ transform: 'scale(1)' }}>
                    {connectors}
                    {bbox}
                    {tokens}
                </div>
            </Comp>
        );
    }
}

export default Grapher;
