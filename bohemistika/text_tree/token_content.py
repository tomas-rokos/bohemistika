from bohemistika.json_tools.get import get as jsonGet

from .token_sub_token_keys import token_sub_token_keys

from .token_schema import TokenSchema


def token_content(tkn: TokenSchema) -> str:
    if isinstance(tkn, str):
        return tkn
    token_contents = []
    for key in token_sub_token_keys(tkn):
        subtkn = jsonGet(tkn, key)
        if isinstance(subtkn, str):
            token_contents.append(subtkn)
        else:
            c = jsonGet(subtkn, 'c')
            token_contents.append(c)
    return ' '.join(token_contents)
