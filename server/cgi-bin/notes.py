#!/usr/bin/env python
import sys
sys.path.insert(0, '../')

from bson.json_util import dumps
import logging

from bohemistika.mongo import Mongo

logging.basicConfig(level=logging.INFO)

print("Content-'type': application/json; charset=utf-8")
print("")

logger = logging.getLogger(__name__)

with Mongo() as client:
    db = client.bohemistika
    notes = db.notes.find({})
    print('[')
    notes_len = 0
    for note in notes:
        notes_len += 1
        if notes_len != 1:
            print(',')
        print(dumps(note))
    print(']')
    logger.info('Dumped %d note(s)', notes_len)
