import json

from .sentence import grapher_sentence


def test_empty_call():
    assert grapher_sentence(None) == []
    assert grapher_sentence({}) == []


def test_one_level():
    assert grapher_sentence({
        't': 's',
        'c': {
            1: 'Tohle',
            2: 'je',
            6: 'test'
        }
    }) == [
        {'key': 'c.1', 'text': 'Tohle', 'pos': {'x': 10, 'y': 10}, 'type': 'subject'},
        {'key': 'c.2', 'text': 'je', 'pos': {'x': 75, 'y': 10}, 'type': 'subject'},
        {'key': 'c.6', 'text': 'test', 'pos': {'x': 113, 'y': 10}, 'type': 'subject'}
    ]


def test_two_level():
    assert grapher_sentence({
        't': 's',
        'c': {
            1: 'Tohle',
            2: {
                't': 'm',
                'c': {
                    3: 'je',
                    4: 'jen',
                }
            },
            6: 'test'
        }
    }) == [
        {'key': 'c.1', 'text': 'Tohle', 'pos': {'x': 10, 'y': 10}, 'type': 'subject'},
        {'key': 'c.2.c.3', 'text': 'je', 'pos': {'x': 75, 'y': 10}, 'type': 'subject'},
        {'key': 'c.2.c.4', 'text': 'jen', 'pos': {'x': 113, 'y': 10}, 'type': 'subject'},
        {'key': 'c.6', 'text': 'test', 'pos': {'x': 160, 'y': 10}, 'type': 'subject'}
    ]


def test_two_level_object_tokens():
    assert grapher_sentence({
        't': 's',
        'c': {
            1: {
                'c': 'Tohle'
            },
            2: {
                't': 'm',
                'c': {
                    3: 'je',
                    4: {
                        'c': 'jen',
                        't': 't'
                    },
                }
            },
            6: 'test'
        }
    }) == [
        {'key': 'c.1', 'text': 'Tohle', 'pos': {'x': 10, 'y': 10}, 'type': 'subject'},
        {'key': 'c.2.c.3', 'text': 'je', 'pos': {'x': 75, 'y': 10}, 'type': 'subject'},
        {'key': 'c.2.c.4', 'text': 'jen', 'pos': {'x': 113, 'y': 10}, 'type': 'subject'},
        {'key': 'c.6', 'text': 'test', 'pos': {'x': 160, 'y': 10}, 'type': 'subject'}
    ]


def test_one_level_with_structure():
    assert grapher_sentence({
        't': 's',
        'c': {
            1: 'Tohle',
            2: {'c': 'je','p': 1},
            6: 'test'
        }
    }) == [
        {'key': 'c.1', 'text': 'Tohle', 'pos': {'x': 10, 'y': 10}, 'type': 'subject'},
        {'key': 'c.2', 'text': 'je', 'pos': {'x': 75, 'y': 10}, 'parent': 'c.1'},
        {'key': 'c.6', 'text': 'test', 'pos': {'x': 113, 'y': 10}, 'type': 'subject'}
    ]


# def test_neptun_sample():
#     with open('samples/neptun_parsey_converted.json') as file_content:
#         assert grapher_sentence(json.loads(file_content.read())) == []
