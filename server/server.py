import http.server
import logging

logging.basicConfig(level=logging.INFO)

logger = logging.getLogger(__name__)


PORT = 8080


class MyServer(http.server.CGIHTTPRequestHandler):
    def do_GET(self):
        logger.info('serving %s', self.path)
        if '.' not in self.path:
            logger.info('redirected to index.html')
            self.path = 'index.html'

        return http.server.CGIHTTPRequestHandler.do_GET(self)


with http.server.HTTPServer(("", PORT), MyServer) as httpd:
    logger.info("serving at port %d", PORT)
    httpd.serve_forever()
