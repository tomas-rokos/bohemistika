import * as _ from 'lodash';
import * as React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { Menu, Icon, Dropdown, Tag } from 'antd';

const Comp = styled.div`
    display: table;
    border: 1px solid grey;
    width: 100%;

    .doc {
        display: table-row;

        > * {
            display: table-cell;
            border: 1px solid lightgray;
            padding: 5px;
        }
        .title {
            text-decoration: none;
            :hover {
                background-color: #eee;
            }
        }
        .stats {
            display: flex;
            flex-direction: row;

            .item {
                margin-right: 10px;
                .icon {
                    font-size: 70%;
                }
                .count {
                    margin-left: 3px;
                    font-size: 80%;
                }
            }
        }
    }
`;

export function DocList(props) {
    if (_.size(props.docs) === 0) {
        return <div>Žádné dokumenty!!</div>;
    }
    const listItems = props.docs.map((doc) =>
        (
            <div className='doc' key={doc['_id']['$oid']}>
                <Link className='title' to={'/note/' + doc['_id']['$oid']}>
                    <div>
                        <strong>{doc.name}</strong>
                        <br />
                        {doc.src}
                    </div>
                </Link>
                <div>
                    {doc.tags.map((tag) => <Tag key={tag}>{tag}</Tag>)}
                    <div className='stats'>
                        <div className='item'>
                            <Icon className='icon' type='dashboard' />
                            <span className='count'>1 345</span>
                        </div>
                        <div className='item'>
                            <Icon className='icon' type='idcard' />
                            <span className='count'>22</span>
                        </div>
                        <div className='item'>
                            <Icon className='icon' type='calendar' />
                            <span className='count'>5</span>
                        </div>
                        <div className='item'>
                            <Icon className='icon' type='environment' /><span className='count'>18</span>
                        </div>
                        <div className='item'>
                            <Icon className='icon' type='edit' />
                            <span className='count'>3</span>
                        </div>
                    </div>
                </div>
            </div>
        )
    );
    return (
        <Comp>
            {listItems}
        </Comp>
    );
}

export default DocList;
