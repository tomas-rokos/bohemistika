import Algorithmia

req = {
    "src": [
        "Cíl akce .",
        "Cílem celé Akce Neptun bylo mystifikovat a ovlivnit veřejné mínění v Československu a v zahraničí (hlavně v Rakousku a Německu) a diskreditovat některé západní politiky . V roce 1965 měla v Německu skončit lhůta promlčení nacistických zločinů . Hlavní cíl akce byl donutit západoněmeckou vládu, aby byla tato lhůta prodloužena . Západní Německo mělo být díky kampani představené jako pokračovatel nacistického Německa . Kromě toho bylo záměrem akce znejistit Západ tím, že mezi nalezenými dokumenty jsou záznamy nacistických donašečů .",
        "Průběh akce .",
        "Dne 20 . června 1964 byly z Prahy na Šumavu dovezeny čtyři bedny, které byly předem připraveny tak, aby vypadaly, že byly 20 let ukryty na dně jezera . Bedny byly naplněny čistým papírem, protože v té době ještě československá rozvědka neměla k dispozici tajné nacistické dokumenty . Ty měla dodat sovětská zpravodajská služba až později . V noci 21 . června 1964 byly tyto bedny tajně uloženy na dno jezera . Několik dní poté na jezeře probíhalo pátrání, které dokumentoval televizní štáb . Redaktoři o zinscenování celého nálezu nevěděli . Ladislav Bittman jako jeden ze svazarmovských potápěčů nasměroval hledače k bedně, která byla nakonec opravdu objevena 3 . července 1964 . Nález byl nahlášen úřadům a iniciativu převzali policejní a vojenští potápěči . Bedny byly vyzvednuty z jezera a za účasti kamer převezeny do Prahy . 16 . července 1964 vydalo Ministerstvo vnitra oficiální zprávu o nálezu nacistických písemností na Šumavě . Oznámení vyvolalo velký ohlas médií, a to i v zahraničí . Následovala kampaň v televizi a v novinách, která vyvrcholila 15 . září 1964 tiskovou konferencí za účasti československého ministra vnitra Lubomíra Štrougala . Ten seznámil veřejnost s obsahem nalezených beden . Dokumenty podle něj obsahovaly písemné důkazy o tom, že Západní Německo a Rakousko jsou prošpikovány přisluhovači nacistického režimu . Tisková konference měla velký ohlas ve světě . Celý nález působil velmi věrohodně a mystifikaci uvěřili jak novináři, tak i veřejnost .",
    ],
    "format": "graph",
    "language": "czech"
}
client = Algorithmia.client('')
algo = client.algo('deeplearning/Parsey/1.1.1')
print(algo.pipe(req).result)
