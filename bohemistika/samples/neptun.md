# Akce Neptun
Akce Neptun je krycí název jedné z nejúspěšnějších dezinformačních kampaní Státní bezpečnosti, při
které byly v roce 1964 na dně Černého jezera na Šumavě senzačně objeveny bedny s tajnými
nacistickými materiály, čehož komunistický režim využil na masivní propagandu v domácích
i světových médiích.

## Cíl akce
Cílem celé Akce Neptun bylo mystifikovat a ovlivnit veřejné mínění v Československu a v zahraničí
(hlavně v Rakousku a Německu) a diskreditovat některé západní politiky. V roce 1965 měla v Německu
skončit lhůta promlčení nacistických zločinů. Hlavní cíl akce byl donutit západoněmeckou vládu, aby
byla tato lhůta prodloužena. Západní Německo mělo být díky kampani představené jako pokračovatel
nacistického Německa. Kromě toho bylo záměrem akce znejistit Západ tím, že mezi nalezenými dokumenty
jsou záznamy nacistických donašečů.

## Příprava akce
Akci Neptun připravilo nové oddělení československé rozvědky, tzv. odbor aktivních opatření.
Tento odbor se zabýval dezinformacemi, propagandou a politickými provokacemi vůči Západu.
Akce byla inspirována nálezem množství falešných anglických liber z nacistické padělatelské
dílny. Ty byly nalezeny na dně jezera Toplitzsee v roce 1963. Příležitost k zinscenování
senzačního nálezu se naskytla, když se redaktoři oblíbeného televizního magazínu Zvědavá
kamera zkoušeli prozkoumat Čertovo a Černé jezero v šumavských lesích. Jedním z potápěčů
Svazu pro spolupráci s armádou, které si televize najala, byl totiž Ladislav Bittman -
zástupce náčelníka dezinformačního oddělení. Bittman se po okupaci Československa stal
jedním z nejznámějších detektivů, který utekl do USA, kde spolupracoval s americkou
tajnou službou, přednášel, publikoval a odhalil pravé pozadí celé akce Neptun.

## Průběh akce
Dne 20. června 1964 byly z Prahy na Šumavu dovezeny čtyři bedny, které byly předem připraveny
tak, aby vypadaly, že byly 20 let ukryty na dně jezera. Bedny byly naplněny čistým papírem,
protože v té době ještě československá rozvědka neměla k dispozici tajné nacistické dokumenty.
Ty měla dodat sovětská zpravodajská služba až později. V noci 21. června 1964 byly tyto bedny
tajně uloženy na dno jezera. Několik dní poté na jezeře probíhalo pátrání, které dokumentoval
televizní štáb. Redaktoři o zinscenování celého nálezu nevěděli. Ladislav Bittman jako jeden ze
svazarmovských potápěčů nasměroval hledače k bedně, která byla nakonec opravdu objevena 3. 
července 1964. Nález byl nahlášen úřadům a iniciativu převzali policejní a vojenští potápěči. Bedny byly
vyzvednuty z jezera a za účasti kamer převezeny do Prahy. 16. července 1964 vydalo Ministerstvo
vnitra oficiální zprávu o nálezu nacistických písemností na Šumavě. Oznámení vyvolalo velký ohlas
médií, a to i v zahraničí. Následovala kampaň v televizi a v novinách, která vyvrcholila 15. září
1964 tiskovou konferencí za účasti československého ministra vnitra Lubomíra Štrougala. Ten seznámil
veřejnost s obsahem nalezených beden. Dokumenty podle něj obsahovaly písemné důkazy o tom, že
Západní Německo a Rakousko jsou prošpikovány přisluhovači nacistického režimu. Tisková konference
měla velký ohlas ve světě. Celý nález působil velmi věrohodně a mystifikaci uvěřili jak novináři,
tak i veřejnost.

## Dopady akce
Akce splnila své cíle. Dokumenty vzrušily světovou veřejnost, zahraniční organizace stíhající
nacistické zločince stupňovaly své úsilí. Mnoho materiálů bylo využito ke kompromitovaní
západoněmeckých, rakouských či italských politiků. Způsobily odchod některých politiků ze
scény i několik sebevražd. Pravé pozadí diskreditační a dezinformační operace odhalil veřejnosti
až v 70. letech samotný Ladislav Bittman.
