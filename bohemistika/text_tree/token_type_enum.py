from enum import Enum


class TokenType(Enum):
    UNKNOWN = ''
    WORD = 'w'
    NUMBER = 'n'
    SPACE = '-'
    PUNCTUATION = '.'
    SPECIALS = '$'

    DOCUMENT = 'doc'
    PARAGRAPH = 'p'
    HEADER1 = 'h1'
    HEADER2 = 'h2'

    SENTENCE = 's'

    KEY = 'k'
    MARK = 'm'
