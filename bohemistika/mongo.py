from pymongo import MongoClient
import logging

class Mongo(object):
    def __init__(self):
        self.db = None
        self.logger = logging.getLogger(__name__)

    def __enter__(self):
        self.db = MongoClient()
        self.logger.info('Database connection established')
        return self.db

    def __exit__(self, type, value, traceback):
        self.db.close()
        self.logger.info('Database connection closed')
