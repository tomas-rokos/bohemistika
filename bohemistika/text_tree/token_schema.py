from voluptuous import Schema, Optional, Self, Any

from .token_type_enum import TokenType

TokenSchema = Schema({
    Optional('t'): TokenType,
    'c': Any(str, {int: Any(str, Self)}),
})
