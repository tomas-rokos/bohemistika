def get(obj, path: str):
    path_keys = path.split('.')
    act = obj
    for key in path_keys:
        if key == '':
            continue
        if not isinstance(act, dict):
            return None
        if str.isdigit(key):
            act = act.get(int(key))
        else:
            act = act.get(key)
        if act is None:
            return None
    return act
