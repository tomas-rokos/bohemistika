import * as _ from 'lodash';
import * as React from 'react';
import styled from 'styled-components';

import Grapher from '../components/grapher';

import useApiCall from '../store/use-api-call';

const Elem = styled.div`
    width: 100%;
    height: 100vh;
`;

export const Index = (props) => {
    const data = useApiCall('graph.py');

    return (
        <Elem>
            <Grapher tokens={data ? data : []}/>
        </Elem>
    );
};

export default Index;
