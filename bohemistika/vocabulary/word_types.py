from enum import Enum


class WordType(Enum):
    SUBSTANTIVUM = 's'          # podstatné jméno
    ADJECTIVUM = 'a'            # přídavné jméno
    PRONOMOEN = 'p'             # zájmeno
    NUMERALE = 'n'              # číslovka
    VERBUM = 'v'                # sloveso
    ABVERBIUM = 'ab'            # příslovce
    PRAEPOSITIO = 'pr'          # předložka
    CONJUNCTIO = 'c'            # spojka
    PARTICULA = 'pa'            # částice
    INTERIECTIO = 'i'           # citoslovce
