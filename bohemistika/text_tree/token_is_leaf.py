from bohemistika.json_tools.get import get as jsonGet

from .token_schema import TokenSchema


def token_is_leaf(tkn: TokenSchema) -> bool:
    c = jsonGet(tkn, 'c')
    if isinstance(c, dict):
        return False
    else:
        return True
