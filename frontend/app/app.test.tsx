import * as React from 'react';
const ShallowRenderer = require('react-test-renderer/shallow');
import { Route } from 'react-router';

import { Appka } from './app';

import Index from '../routes/index';
import Graph from '../routes/graph';

/**
 * Just a simple integration tests
 */

test('Appka', () => {
    const renderer = new ShallowRenderer();
    renderer.render(<Appka />);
    const result = renderer.getRenderOutput();
    expect(result).toEqual(
        <React.Fragment>
            <Route path='/' exact={true} component={Index} />
            <Route path='/note/:docid' component={Graph} />
        </React.Fragment>
    );
});

