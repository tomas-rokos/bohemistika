import sys
sys.path.insert(0, './')
import datetime
import json
import logging

from bohemistika.mongo import Mongo

logging.basicConfig(level=logging.INFO)

logger = logging.getLogger(__name__)

with Mongo() as client:
    logger.info('dropping existing data ...')
    client.drop_database('bohemistika')
    db = client.bohemistika
    logger.info('database dropped .. filling lemmas ... ')

    with open('scripts/init_data/lemmatization-cs.txt', 'r') as text_file:
        lines = text_file.readlines()
        for line in lines:
            words = line.split('\t')
            if len(words) != 2:
                logger.warning('No two words in this line: ' + line)
                continue
            db.vocabulary.insert_one({'word': words[1].strip().lower(), 'master': words[0].strip().lower()})

    logger.info('lemmas loaded .. creating sample notes ... ')

    note_id = db.notes.insert_one({
        'name': 'Neptun - Wikipedia',
        'src': 'https://cs.wikipedia.org/wiki/Akce_Neptun',
        'tags': ['wikipedia', 'stb'],
        'createdAt': datetime.datetime.utcnow()
    }).inserted_id

    with open('bohemistika/samples/neptun.md', 'r') as text_file:
        content = text_file.read()
        original_id = db.originals.insert_one({
            'content': content,
            'noteId': note_id,
            'createdAt': datetime.datetime.utcnow()
        }).inserted_id

    with open('bohemistika/samples/neptun_parsey_converted.json', 'r') as text_file:
        content = json.loads(text_file.read())
        tree_id = db.trees.insert_one({
            'content': content,
            'originalId': original_id,
            'noteId': note_id,
            'createdAt': datetime.datetime.utcnow()
        }).inserted_id

    logger.info('neptun created as ' + str(note_id))
