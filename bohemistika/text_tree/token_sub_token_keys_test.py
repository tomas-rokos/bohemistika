from .token_sub_token_keys import token_sub_token_keys


def test_empty_inputs():
    assert token_sub_token_keys(None) == ['']
    assert token_sub_token_keys({}) == ['']


def test_leaf_token():
    assert token_sub_token_keys({'c': 'test'}) == ['']


def test_simple_level():
    assert token_sub_token_keys({'c': {0: 'z1', 4: 'z2'}}) == ['c.0', 'c.4']


def test_nested_levels():
    assert token_sub_token_keys({'c': {0: {'c': {3: 'z1', 5: 'z2'}}, 4: {'c': {2: 'z1', 6: 'z2'}}}}) \
           == ['c.0.c.3', 'c.0.c.5', 'c.4.c.2', 'c.4.c.6']


def test_top_level_only():
    assert token_sub_token_keys({'c': {0: {'c': {3: 'z1', 5: 'z2'}}, 4: {'c': {2: 'z1', 6: 'z2'}}}},
                                top_level_only=True) \
           == ['c.0', 'c.4']
