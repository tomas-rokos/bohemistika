from .token_is_leaf import token_is_leaf


def test_single_level_string_token():
    assert token_is_leaf('test') == True


def test_single_level_object_token():
    assert token_is_leaf({'c': 'test'}) == True


def test_multi_level_object_token():
    assert token_is_leaf({'c': {'a': 'test'}}) == False
