import unittest

from .markup import markup, TokenType


class TokenizerMarkupTestCase(unittest.TestCase):
    def test_empty(self):
        self.assertEqual({}, markup(''))
        self.assertEqual({}, markup('   '))
        self.assertEqual({}, markup(' \n  \n  '))

    def test_one_paragraph(self):
        self.assertEqual({0: {'c': {0: 'a', 2: 'a', 4: 'b', 7: 'b', 9: 'b', 11: 'c'}, 't': TokenType.PARAGRAPH}
                          },
                         markup('a a b\n b b c'))
        self.assertEqual({2: {'c': {0: 'a', 2: 'a', 4: 'b', 7: 'b', 9: 'b', 11: 'c'}, 't': TokenType.PARAGRAPH}
                          },
                         markup(' \n  \na a b\n b b c\n  \n  '))

    def test_two_paragraphs(self):
        self.assertEqual({0: {'c': {0: 'a', 2: 'a', 4: 'b', 7: 'b', 9: 'b', 11: 'c'}, 't': TokenType.PARAGRAPH},
                          3: {'c': {0: 'd', 2: 'e', 4: 'f'}, 't': TokenType.PARAGRAPH},
                         },
                         markup('a a b\n b b c\n\nd e f '))
        self.assertEqual({2: {'c': {0: 'a', 2: 'a', 4: 'b', 7: 'b', 9: 'b', 11: 'c'}, 't': TokenType.PARAGRAPH},
                          5: {'c': {0: 'd', 2: 'e', 4: 'f'}, 't': TokenType.PARAGRAPH},
                          },
                         markup(' \n  \na a b\n b b c\n  \nd e f\n \n  \n  '))

    def test_header_1(self):
        self.assertEqual({0: {'c': {0: 'header'}, 't': TokenType.HEADER1},
                         },
                         markup('# header'))
        self.assertEqual({1: {'c': {0: 'header'}, 't': TokenType.HEADER1},
                         },
                         markup('\n  # header  \n \n  '))

    def test_header_2(self):
        self.assertEqual({0: {'c': {0: 'header'}, 't': TokenType.HEADER2},
                         },
                         markup('## header'))
        self.assertEqual({1: {'c': {0: 'header'}, 't': TokenType.HEADER2},
                         },
                         markup('\n ## header \n  \n'))

    def test_complex_text(self):
        self.assertEqual({0: {'c': {0: 'header'}, 't': TokenType.HEADER2},
                         },
                         markup('## header'))


if __name__ == '__main__':
    unittest.main()

