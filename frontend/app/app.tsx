import * as React from 'react';
import { withRouter, Route } from 'react-router';

import Index from '../routes/index';
import Graph from '../routes/graph';

export const Appka = (props) => {
    return (
        <>
            <Route path='/' exact={true} component={Index} />
            <Route path='/note/:docid' component={Graph} />
        </>
    );
};

export default withRouter(Appka);
