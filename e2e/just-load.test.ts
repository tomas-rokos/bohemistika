import * as puppeteer from 'puppeteer';

test('simple load', async () => {
    const browser = await puppeteer.launch({
        headless: true,
        slowMo: 300
    });
    const page = await browser.newPage();

    await page.emulate({
        viewport: {
            width: 500,
            height: 2400
        },
        userAgent: ''
    });

    await page.goto('http://localhost:8080');

    // const btn = await page.waitForSelector('.cta-primary');
    // await btn.click();
    //
    // await page.click('.close');
    //
    // await page.click('.ui-btn');
    //
    // await btn.click();

    await browser.close();
}, 16000);
