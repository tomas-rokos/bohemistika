import * as _ from 'lodash';
import { useEffect, useState } from 'react';

export default function useApiCall<P>( pnt: string) {
    const [value, setValue] = useState(null);

    useEffect(() => {
        console.log('rq API: ' + pnt);

        fetch('http://localhost:8080/cgi-bin/' + pnt).then((response) => {
            return response.json();
        }).then((myJson) => {
            setValue(myJson);
            console.log('res API: ' + pnt, myJson);
        });
        return () => {
        };
    }, [pnt]);

    return value;
}
