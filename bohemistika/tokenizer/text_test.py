import unittest

from .text import *


class TokenizerTextTestCase(unittest.TestCase):
    def test_empty(self):
        self.assertEqual({}, text(''))

    def test_one_token(self):
        self.assertEqual({'a': 'test'}, text('test'))
    #     self.assertEqual({0: {'c': '456', 't': TokenType.NUMBER}}, text('456'))
    #
    # def test_more_tokens(self):
    #     self.assertEqual({0: 'test', 5: 'few', 9: 'mores'}, text('test few mores'))
    #     self.assertEqual({0: 'test', 5: {'c': '2', 't': TokenType.NUMBER}, 7: 'mores'}, text('test 2 mores'))
    #
    # def test_white_spacing(self):
    #     self.assertEqual({2: 'This', 7: 'is', 13: 'a', 16: 'white', 22: 'space', 30: 'test'},
    #                      text('  This is    a  white space\n  test'))
    #
    # def test_more_token_types(self):
    #     self.assertEqual({0: 'This',
    #                       5: 'is',
    #                       8: {'c': '2', 't': TokenType.NUMBER},
    #                       9: {'c': '.', 't': TokenType.PUNCTUATION},
    #                       10: {'c': '3', 't': TokenType.NUMBER},
    #                       12: 'test',
    #                       18: 'of',
    #                       21: 'x',
    #                       22: {'c': "'", 't': TokenType.UNKNOWN},
    #                       24: 'tokens'
    #                       },
    #                      text('This is 2.3 test\n of x\' tokens'))


if __name__ == '__main__':
    unittest.main()

