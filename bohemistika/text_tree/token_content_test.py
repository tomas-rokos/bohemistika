from .token_content import token_content


def test_single_level_string_token():
    assert token_content('test') == 'test'


def test_single_level_object_token():
    assert token_content({'c': 'test'}) == 'test'


def test_multi_level_object_token():
    assert token_content({'c': {'a': 'test'}}) == 'test'
    assert token_content({'c': {'a': 'test', 'b': 'try'}}) == 'test try'
    assert token_content({'c': {'a': {'c': 'test'}, 'b': 'try'}}) == 'test try'
