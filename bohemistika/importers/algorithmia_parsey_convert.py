import json

from bohemistika.text_tree.token_schema import TokenSchema, TokenType


def algorithmia_parsey_convert(parsey_json: dict) -> TokenSchema:
    if parsey_json is None:
        return None
    b_o = parsey_json.get('batchOutput')
    if b_o is None:
        return None
    res = {}
    batch_i = 0
    for batch in b_o:
        sntcs = {}
        sentence_i = 0
        for sentence in batch['sentences']:
            snt = _parse_sentence(sentence)
            sntcs[sentence_i] = snt
            sentence_i += 1
        res[batch_i] = {'t': 'p','c': sntcs}
        batch_i += 1
    final = {'t': 'doc', 'c': res}
    return final


def _parse_sentence(sent: dict) -> dict:
    res = {}
    for node in sent['nodes']:
        res[node['id']] = {'c': node['form'] }
    for node in sent['edges']:
        res[node['target']]['p'] = node['source']
    return {'t': 's', 'c': res}
