import unittest

from .get import get

sample = {
    'a': 'va',
    'b': {
        'ba': {
            'baa': 'vbaa',
            'bab': 'vbab'
        },
        'bb': 'vbb',
        5: 'vb5'
    }
}


class JSONToolsGetTestCase(unittest.TestCase):
    def test_direct_subnode(self):
        self.assertEqual(get(sample, 'a'), 'va')

    def test_non_existing_direct_subnode(self):
        self.assertEqual(get(sample, 'c'), None)

    def test_nested_subnode(self):
        self.assertEqual(get(sample, 'b.ba.baa'), 'vbaa')
        self.assertEqual(get(sample, 'b.bb'), 'vbb')
        self.assertEqual(get(sample, 'b.ba'), {'baa': 'vbaa', 'bab': 'vbab'})

    def test_nested_numerical_key_subnode(self):
        self.assertEqual(get(sample, 'b.5'), 'vb5')

    def test_non_existing_nested_subnode(self):
        self.assertEqual(get(sample, 'b.ba.baa.baaaa'), None)
        self.assertEqual(get(sample, 'b.ba.bac'), None)
        self.assertEqual(get(sample, 'b.bc'), None)
        self.assertEqual(get(sample, 'c.cc'), None)

    def test_empty_key(self):
        assert get('ahoj', '') == 'ahoj'
        assert get({'c': 'ahoj'}, '') == {'c': 'ahoj'}


if __name__ == '__main__':
    unittest.main()
