from bohemistika.json_tools.get import get as jsonGet
from bohemistika.text_tree.token_schema import TokenSchema, TokenType
from bohemistika.text_tree.token_is_leaf import token_is_leaf


def token_sub_token_keys(tkn: TokenSchema, top_level_only=False) -> []:
    result = []
    __private_recursion_collecting(tkn, result, '', top_level_only)
    return result


def __private_recursion_collecting(tkn: TokenSchema, collected_keys: list, parent: str = '', top_level_only=False):
    if token_is_leaf(tkn) or (top_level_only and parent != ''):
        collected_keys.append(parent)
        return
    sub_dict: dict = jsonGet(tkn, 'c')
    for key in sub_dict.keys():
        sub_key = 'c.' + str(key)
        sub_tkn = jsonGet(tkn, sub_key)
        __private_recursion_collecting(sub_tkn, collected_keys, sub_key if parent == '' else parent + '.' + sub_key,
                                       top_level_only)
