#!/usr/bin/env python
import sys
sys.path.insert(0, '../')

import json
import logging

from bohemistika.graphers.sentence import grapher_sentence

logging.basicConfig(level=logging.INFO)

print("Content-'type': application/json; charset=utf-8")
print("")

logger = logging.getLogger(__name__)

nodes = grapher_sentence({
        't': 's',
        'c': {
            1: {
                'c': 'Tohle'
            },
            2: {
                't': 'm',
                'c': {
                    3: 'je',
                    4: {
                        'c': 'jen',
                        't': 't'
                    },
                }
            },
            6: 'test'
        }
    })

# nodes = [
#     {'key': 'a', 'text': 'Akce Neptun', 'pos': {'x': 20, 'y': 200}, 'type': 'subject'},
#     {'key': 'b', 'text': 'odbor aktivních opetření', 'pos': {'x': 100, 'y': 100}, 'parent': 'a'},
#     {'key': 'c', 'text': 'Státní bezpečnost', 'pos': {'x': 300, 'y': 20}, 'parent': 'b'},
#     {'key': 'd', 'text': 'zabýval', 'pos': {'x': 330, 'y': 150}, 'parent': 'b'},
#     {'key': 'e', 'text': 'dezinformacemi', 'pos': {'x': 430, 'y': 110}, 'parent': 'd'},
#     {'key': 'f', 'text': 'propagandou', 'pos': {'x': 430, 'y': 170}, 'parent': 'd'},
#     {'key': 'g', 'text': 'polickými provokacemi', 'pos': {'x': 430, 'y': 230}, 'parent': 'd'},
#     {'key': 'h', 'text': 'inspirována', 'pos': {'x': 100, 'y': 300}, 'parent': 'a'},
#     {'key': 'i', 'text': 'nálezem', 'pos': {'x': 220, 'y': 400}, 'parent': 'h'},
#     {'key': 'j', 'text': 'falešných liber', 'pos': {'x': 350, 'y': 320}, 'parent': 'i'},
#     {'key': 'k', 'text': 'z nacistické dílny', 'pos': {'x': 350, 'y': 370}, 'parent': 'i'},
#     {'key': 'l', 'text': 'na dně', 'pos': {'x': 350, 'y': 420}, 'parent': 'i'},
#     {'key': 'm', 'text': 'jezero Toplitzsee', 'pos': {'x': 450, 'y': 420}, 'parent': 'l'},
#     {'key': 'n', 'text': 'v roce 1963', 'pos': {'x': 350, 'y': 470}, 'parent': 'i'},
# ]

print(json.dumps(nodes))
