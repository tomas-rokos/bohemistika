from bohemistika.json_tools.get import get as jsonGet

from .token_type_enum import TokenType
from .token_schema import TokenSchema


def token_type(tkn: TokenSchema) -> TokenType:
    if isinstance(tkn, str):
        return TokenType.WORD
    t = jsonGet(tkn, 't')

    if t is None:
        return TokenType.WORD
    else:
        return t
