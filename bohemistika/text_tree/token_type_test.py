from .token_type import token_type, TokenType


def test_single_level_string_token():
    assert token_type('aaa') == TokenType.WORD


def test_single_level_word_object_token():
    assert token_type({'c': 'test'}) == TokenType.WORD


def test_single_level_other_object_token():
    assert token_type({'t': TokenType.NUMBER, 'c': '23'}) == TokenType.NUMBER
