from .text import text, breakers, TokenType


def markup(txt: str):
    tokenized: dict = {}

    act_string = ''
    act_start = 0

    lines = txt.split('\n')
    lines.append('')
    for i in range(len(lines)):
        line = lines[i]
        stripped_line = line.strip()
        if len(stripped_line) == 0:
            if len(act_string) != 0:
                tokenized[act_start] = {
                    't': TokenType.PARAGRAPH,
                    'c': text(act_string)
                }
                act_string = ''
                continue
        else:
            if len(act_string) != 0:
                act_string += '\n' + line
            else:
                if stripped_line.startswith('#') and ord(stripped_line[1]) in breakers:
                    tokenized[i] = {
                        't': TokenType.HEADER1,
                        'c': text(stripped_line[2:])
                    }
                elif stripped_line.startswith('##') and ord(stripped_line[2]) in breakers:
                    tokenized[i] = {
                        't': TokenType.HEADER2,
                        'c': text(stripped_line[3:])
                    }
                else:
                    act_start = i
                    act_string = line
    return tokenized

