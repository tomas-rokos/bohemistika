from bohemistika.json_tools.get import get
from bohemistika.text_tree import token_sub_token_keys, token_is_leaf, token_content, TokenSchema, TokenType

from .graph_schema import GraphSchema
from .get_approx_width import get_approx_width


class __Impl:
    def __init__(self):
        self.nodes = {}

    def add_node(self, key: str, text: str, parent: str = None):
        node = {'key': key, 'text': text}
        if parent is not None:
            node['parent'] = 'c.' + str(parent)
        else:
            node['type'] = 'subject'
        key_parts = key.split('.')
        self.nodes.setdefault(int(key_parts.pop()), node)

    def align(self):
        curr_x = 10
        for key in self.nodes.keys():
            self.nodes[key].setdefault('pos', {'x': curr_x, 'y': 10})
            curr_x += get_approx_width(self.nodes[key]['text']) + 20
        return


def grapher_sentence(tkn: TokenSchema) -> GraphSchema:
    if token_is_leaf(tkn):
        return []
    result: __Impl = __Impl()
    for key in token_sub_token_keys(tkn):
        token = get(tkn, key)
        result.add_node(key, token_content(token), get(token, 'p'))

    result.align()
    return list(result.nodes.values())
