# inspiration
# https://stackoverflow.com/questions/16007743/roughly-approximate-the-width-of-a-string-of-text-in-python


key_width = 9

def get_approx_width(st: str) -> int:
    size = 0 # in milinches
    for s in st:
        if s in 'lij|\' ':
            size += key_width
        elif s in '![]fI.,:;/\\t':
            size += key_width
        elif s in '`-(){}r"':
            size += key_width
        elif s in '*^zcsJkvxy':
            size += key_width
        elif s in 'aebdhnopqug#$L+<>=?_~FZT0123456789':
            size += key_width
        elif s in 'BSPEAKVXY&UwNRCHD':
            size += key_width
        elif s in 'QGOMm%W@':
            size += key_width
        else:
            size += key_width
    return size
