from voluptuous import Schema, Optional, Any

GraphSchema = Schema(
    [
        {
            'key': str,
            'text': str,
            'pos': {
                'x': int,
                'y': int,
            },
            Optional('parent'): str,
            Optional('type'): Any('subject')
        }
    ]
)
