import * as React from 'react';
import styled from 'styled-components';
import { Vector } from '../../math/vector';

const Elem = styled.div<{ selected: boolean }>`
    position: absolute;
    border-bottom: 2px solid black;
    padding: 5px;
    display: inline-block;
    cursor: move;
    user-select: none;
    background-color: ${props => props.selected ? 'lightgrey' : 'white'}
`;

export function Node(props: {
    text: string;
    onMouseDown;
    pos: Vector;
    selected: boolean;
}, ref) {
    return (
        <Elem
            ref={ref}
            style={{ left: props.pos.x, top: props.pos.y }}
            onMouseDown={props.onMouseDown}
            selected={props.selected}
        >
            {props.text.split(' ').join('\u00A0')}
        </Elem>
    );
}

export default React.forwardRef(Node);
